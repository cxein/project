"""
Функция принимает на вход три целых числа в следующем порядке: день, месяц, год. Функция должна вернуть строку формата '31 февраля 2023 года'.
Пример: вход: 19, 11, 2023. выход: '19 ноября 2023 года'
"""


def format_date(date, month, year):
    month_list = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября',
                  'ноября', 'декабря']

    month_str = month_list[month - 1] if 1 <= month <= 12 else 'Ошибка: введите значение месяца от 1 до 12'
    date_str = date if 1 <= date <= 31 else 'Ошибка: введите значение даты от 1 до 31'
    formatted_date = f'{date_str} {month_str} {year} года'

    return formatted_date


date = 19
month = 11
year = 2023
result = format_date(date, month, year)
print(result)

"""
Функция принимает на вход кортеж строк (имена). Функция должна вернуть словарь, в котором будет указано, какое имя сколько раз встречалось в кортеже.
Пример: вход: ('Олег', 'Игорь', 'Анна', 'Анна', 'Игорь', 'Анна', 'Василий') Выход: {'Олег': 1, 'Игорь': 2, 'Анна': 3, 'Василий': 1}
"""


def count_names(names_tuple):
    name_count = {}

    for name in names_tuple:
        if name in name_count:
            name_count[name] += 1
        else:
            name_count[name] = 1

    return name_count


input_names = ('Олег', 'Игорь', 'Анна', 'Анна', 'Игорь', 'Анна', 'Василий')
result = count_names(input_names)
print(result)

"""
Функция принимает на вход словарь. В словаре находятся данные об имени человека в формате {'first_name': 'Имя', 'last_name': 'Фамилия', 'middle_name': 'Отчество'}. 
Функция должна вернуть данные о человеке в виде строки формата 'Фамилия Имя Отчество'. 
Данные в словаре могут быть неполными, в этом случае:   

      если отсутствует фамилия, выводятся имя и отчество.
      если отсутствует отчество - выводятся фамилия и имя (при наличии).
      если отсутствует имя, выводится только фамилия, отчество без имени выводить не надо. 
      если в словаре есть только отчество или вообще нет элементов - выводится строка 'Нет данных'.

      Пример: вход: {'first_name': 'Иван', 'last_name': 'Иванов'} выход: 'Иванов Иван'
"""


def format_name(data):
    if not data:
        return 'Нет данных'

    first_name = data.get('first_name', '')
    last_name = data.get('last_name', '')
    middle_name = data.get('middle_name', '')

    if not first_name and not last_name:
        return 'Нет данных'

    if not first_name:
        return last_name

    if not last_name:
        if middle_name:
            return f'{first_name} {middle_name}'
        else:
            return first_name

    if middle_name:
        return f'{last_name} {first_name} {middle_name}'
    else:
        return f'{last_name} {first_name}'


data = {'last_name': 'Ivanov', 'middle_name': 'Ivanovich'}
result = format_name(data)
print(result)

"""
Функция принимает на вход число. Функция должна вернуть True, если число простое (делится только на себя и на 1), и False, если число составное.
"""


def is_prime(number):
    if number < 2:
        return False
    for i in range(2, int(number ** 0.5) + 1):
        if number % i == 0:
            return False
    return True


num = int(input("Введите число: "))
result = is_prime(num)

if result:
    print(f"{num} - это простое число")
else:
    print(f"{num} - это составное число")

"""
Функция принимает на вход произвольное количество аргументов (*args). Аргументы могут быть разных типов - числа, строки, 
булевы типы, None. Функция должна вернуть список, в который войдут все уникальные числа, встреченные во входящих 
аргументах, в возрастающем порядке.
Пример: вход: 1, '2', 'text', 42, None, None, None, 15, True, 1, 1. Выход: [1, 15, 42].
Подсказка: isinstance
"""


def unique_numbers(*args):
    unique_set = set()

    for arg in args:
        if isinstance(arg, (int, float)):
            unique_set.add(arg)

    unique_list = sorted(list(unique_set))
    return unique_list


input_data = (1, '2', 'text', 42, None, None, None, 15, True, 1, 1.0)
result = unique_numbers(*input_data)
print(result)
