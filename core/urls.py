from django.urls import path

from core.views import Homepage, Info, Authors, Trips, Articles, Comments, Author, Article, CreateComment

urlpatterns = [
    path('', Homepage.as_view(), name='homepage'),
    path('info/', Info.as_view(), name='info'),
    path('authors/', Authors.as_view(), name='authors'),
    path('trips/', Trips.as_view(), name='trips'),
    path('articles/', Articles.as_view(), name='articles'),
    path('comments/', Comments.as_view(), name='comments'),
    path('author/<id>/', Author.as_view(), name='author'),
    path('article/<id>/', Article.as_view(), name='article'),
    path('create_comment/', CreateComment.as_view(), name='create_comment')
]
