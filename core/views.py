from django.db.models import Count, Q
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, DetailView, FormView

from core import models, forms


# Create your views here.
class Homepage(View):
    def get(self, request):
        context = {'text': 'Форум для путешественников', 'title': 'Главная страница'}
        return render(request=request, template_name='core/mainpage.html', context=context)


class Info(View):
    def get(self, request):
        context = {'title': 'О нас'}
        return render(request=request, template_name='core/info.html', context=context)


class Authors(ListView):
    template_name = 'core/authors.html'
    extra_context = {'title': 'Авторы'}
    context_object_name = 'authors'

    def get_queryset(self):
        queryset = models.Author.objects.all()

        sort_param = self.request.GET.get('sort_by')
        if sort_param == 'last_name':
            queryset = queryset.order_by('last_name')
        elif sort_param == 'articles':
            queryset = queryset.annotate(articles_quantity=Count('articles')).order_by('-articles_quantity')

        search_name = self.request.GET.get('search_name')
        if search_name:
            queryset = queryset.filter(
                Q(last_name__contains=search_name) |
                Q(first_name__contains=search_name) |
                Q(middle_name__contains=search_name)
            )

        return queryset



class Trips(View):
    def get(self, request):
        trips = models.Trip.objects.all()
        context = {'title': 'Поездки', 'trips': trips}
        return render(request=request, template_name='core/trips.html', context=context)


class Articles(ListView):
    template_name = 'core/articles.html'
    extra_context = {'title': 'Статьи'}
    context_object_name = 'articles'
    queryset = models.Article.objects.order_by('name')



class Comments(View):
    def get(self, request):
        comments = models.Comment.objects.all()
        context = {'title': 'Комментарии', 'comments': comments}
        return render(request=request, template_name='core/comments.html', context=context)


class Author(View):
    def get(self, request, id):
        author = models.Author.objects.get(id=id)
        context = {'title': author, 'author': author}
        return render(request=request, template_name='core/author.html', context=context)


class Article(DetailView):
    template_name = 'core/article.html'
    model = models.Article
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        article = self.object
        return super().get_context_data(title=article.name)

"""class CreateComment(View):
    def get(self, request, *args, **kwargs):
        form = kwargs.get('form') or forms.Comment()
        return render(request, template_name='core/create_comment.html',
                      context={'title': 'Новый комментарий', 'form': form})

    def post(self, request, *args, **kwargs):
        form = forms.Comment(request.POST)
        if form.is_valid():
            author = form.cleaned_data.get('author')
            article = form.cleaned_data.get('article')
            text = form.cleaned_data.get('text')
            comment = models.Comment(text=text)
            if author:
                comment.author = author
            if article:
                comment.article = article
            comment.save()
            return redirect('comments')
        
"""

class CreateComment(FormView):
    template_name = 'core/create_comment.html'
    form_class = forms.Comment
    success_url = reverse_lazy('comments')

    def form_valid(self, form):
        form.save()
        return redirect(self.get_success_url())
