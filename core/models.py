from django.db import models

class Author(models.Model):
    first_name = models.CharField('Имя', max_length=50, blank=True)
    last_name = models.CharField('Фамилия', max_length=50)
    middle_name = models.CharField('Отчество', max_length=50, blank=True)
    email = models.CharField('Почта', max_length=50, blank=True)
    country = models.CharField('Страна', max_length=100, blank=True)
    city = models.CharField('Город', max_length=100, blank=True)
    dob = models.DateTimeField('Дата рождения', blank=True)
    dc = models.DateTimeField('Дата регистрации', auto_now_add=True)

    def __str__(self):
        return self.format_name()

    def format_name(self):

        first_name = self.first_name
        last_name = self.last_name
        middle_name = self.middle_name

        if not first_name and not last_name:
            return 'Нет данных'

        if not first_name:
            return last_name

        if not last_name:
            if middle_name:
                return f'{first_name} {middle_name}'
            else:
                return first_name

        if middle_name:
            return f'{last_name} {first_name} {middle_name}'
        else:
            return f'{last_name} {first_name}'

class Trip(models.Model):
    author = models.ForeignKey(Author, verbose_name='Автор', on_delete=models.CASCADE, related_name='trips')
    country_trip = models.CharField('Страна путешествия', max_length=100, blank=True)
    city_trip = models.CharField('Город путешествия', max_length=100, blank=True)
    date_trip = models.DateTimeField('Дата начала поездки', blank=True)
    date_end_trip = models.DateTimeField('Дата начала поездки', blank=True)

    def __str__(self):
        return self.country_trip

class Article(models.Model):
    author = models.ForeignKey(Author, verbose_name= 'Автор', on_delete=models.CASCADE, related_name='articles')
    trip = models.ForeignKey(Trip, verbose_name= 'Поездки', on_delete=models.CASCADE, related_name='articles')
    name = models.CharField('Название статьи', max_length=300)
    url = models.URLField('url', blank=True)
    dc = models.DateTimeField('Дата создания', auto_now_add=True)
    text = models.TextField('Статья')

    def __str__(self):
        return self.name

class Comment(models.Model):
    author = models.ForeignKey(Author, verbose_name='Автор', on_delete=models.CASCADE, related_name='comments')
    article = models.ForeignKey(Article, verbose_name='Статьи', on_delete=models.CASCADE, related_name='comments')
    url_article = models.URLField('url статьи, под которой оставлен комментарий', blank=True)
    dc_comment = models.DateTimeField('Дата создания комментария', auto_now_add=True)
    text = models.TextField('Комментарий')

    def __str__(self):
        return self.text

