from django import forms
from core.models import Author, Article
from core import models

"""class Comment(forms.Form):
    author = forms.ModelChoiceField(queryset=Author.objects.all(), empty_label=None, label='Выберите автора')
    article = forms.ModelChoiceField(queryset=Article.objects.all(), empty_label=None, label='Выберите статью')
    text = forms.CharField(label='Ваш комментарий', widget=forms.Textarea)
"""

class Comment(forms.ModelForm):
    class Meta:
        model = models.Comment
        fields = ('author', 'article', 'text')

    def clean_text(self):
        text = self.cleaned_data['text']
        if not text:
            raise forms.ValidationError("Поле 'text' не может быть пустым.")
        return self.cleaned_data['text']




